import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Person} from '../domain/person';
import 'rxjs/Rx';

@Injectable()
export class PersonService {
    
    constructor(private http: Http) {}

    getPersons() {
        return this.http.get('app/resources/persons.json')
                    .toPromise()
                    .then(res => <Person[]> res.json().data)
                    .then(data => { return data; });
    }

}