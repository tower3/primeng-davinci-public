import {OnInit} from 'angular2/core';
import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import {InputText} from 'primeng/primeng';
import {Person} from './domain/person';
import {HTTP_PROVIDERS}    from 'angular2/http';
import {DataTable} from 'primeng/primeng';
import {TabView} from 'primeng/primeng';
import {TabPanel} from 'primeng/primeng';
import {Column} from 'primeng/primeng';
import {Header} from 'primeng/primeng';
import {PersonService} from './service/personservice';
import {Footer} from 'primeng/primeng';
import {Button} from 'primeng/primeng';
import {Dialog} from 'primeng/primeng';

@Component({
	selector: 'my-app',
	templateUrl: 'app/app.component.html',
    directives: [DataTable, Column, TabPanel, TabView, Header, Footer, Dialog, Button, InputText, ROUTER_DIRECTIVES],
    providers: [HTTP_PROVIDERS, PersonService]
})
export class AppComponent implements OnInit {

    persons: Person[];

    cols: any[];

    text: string;

    displayablePerson: Person = new DisplayablePerson();

    displayDialog: boolean;

    selectedPerson: Person;

    newPerson: boolean;

    constructor(private personService: PersonService) { }

    ngOnInit() {
        this.personService.getPersons().then(persons => this.persons = persons);
        
        this.cols = [
            { field: 'cardId', header: 'Card ID' },
            { field: 'name', header: 'Name' },
            { field: 'surname', header: 'Surname' },
            { field: 'country', header: 'Country' }
        ];
    }

    showDialogToAdd() {
        this.newPerson = true;
        this.displayablePerson = new DisplayablePerson();
        this.displayDialog = true;
    }

    save() {
        if (this.newPerson)
            this.persons.push(this.displayablePerson);
        else
            this.persons[this.findSelectedPersonIndex()] = this.displayablePerson;

        this.displayablePerson = null;
        this.displayDialog = false;
    }

    showDialogToEdit() {
        this.displayDialog = true;
    }

    delete() {
        this.persons.splice(this.findSelectedPersonIndex(), 1);
        this.displayablePerson = null;
        this.selectedPerson = null;
        this.displayDialog = false;
    }

    onRowSelect(event) {
        this.newPerson = false;
        this.displayablePerson = this.clonePerson(event.data);
    }

    findSelectedPersonIndex(): number {
        return this.persons.indexOf(this.selectedPerson);
    }

    clonePerson(c: Person): Person {
        let displayablePerson = new DisplayablePerson();
        for (let prop in c) {
            displayablePerson[prop] = c[prop];
        }
        return displayablePerson;
    }


}

class DisplayablePerson implements Person {

    constructor(public cardId?, public name?, public surname?, public country?) { }
}